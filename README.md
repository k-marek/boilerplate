# Project name
## start frontend app
`API_HOST=<your_ip> docker-compose up --build`

### `npm run lint`

gives list of errors to styling and js rules

`npm run lint -- ling erorrs and print rest of errors to correct

`docker exec -it enigma_exercise_frontend_dev_1 sh -c "npm run lint"`

### `npm run format`

`docker exec -it enigma_exercise_frontend_dev_1 sh -c "npm run format; npm run lint -- --fix"`

### cleaning linter

`docker exec -it enigma_exercise_frontend_dev_1 sh -c "npm run format; npm run lint -- --fix"`

### lighthouse

You can run locally ligthouse-ci by using dev tools in chrome

