#!/bin/sh
set -x
mkdir -p public
echo "window['runConfig'] = { apiUrl: "http://localhost:5000", refreshRate: 2000}" > public/runtime-config.js

if [ $1 = production ];
then
    mv public/runtime-config.js /usr/share/nginx/html/runtime-config.js
    nginx -g "daemon off;"
else
    npm run start
fi
