import React from "react";
import { HashRouter as Router } from "react-router-dom";

const Home = () => {
  return (
    <Router>
      <header>
        <div>
          <h1>Hello World</h1>
        </div>
      </header>
    </Router>
  );
};

export default Home;
