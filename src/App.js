import React from "react";
import "./App.css";
import { Route, Switch, HashRouter as Router } from "react-router-dom";

import Home from "./Components/Home";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
